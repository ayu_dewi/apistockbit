<?php
//------------------------------------------------------------------
//Retrieve JSON File
header("Content-Type: application/json; charset=UTF-8");
$input = trim(file_get_contents("php://input"));
$datas = json_decode($input, false);

//------------------------------------------------------------------
//Set variable
foreach ($datas as $data)
{
	$v_pe_code = $data->pe_code;
	$v_signature = $data->signature;
	$v_api_type = $data->api_type;
	$v_registration_date = $data->registration_date;
}

$v_check = true;
$v_RSLT_ID = "0";
$v_RSLT_MSG = "";

//Pagination
$v_url = str_replace(".php","","https://trading.simasnet.com" . $_SERVER['PHP_SELF']);
//pro : "https://trading.simasnet.com/APIStockBit/ActivationStatus/ActivationStatus.php";
//dev : "https://trading.simasnet.com/APIStockBit_Dev/ActivationStatus/ActivationStatus.php";

//------------------------------------------------------------------
// Turn off all error reporting & handle exception
error_reporting(0);
set_error_handler("customError", E_ALL);

function customError($errno, $errstr)
{
	if (strlen(trim($errstr))>0) {
		//Pagination
		$v_limit = 100;
		$v_page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		if (($v_page < 1) || (empty($v_page))) {
			$v_page = 1;
		}
		$v_offset = (($v_page - 1) * $v_limit);
		$v_seq = 0;

		//Pagination
		$v_totalpage = 1;
		if ($v_page > $v_totalpage) {
			$v_page = $v_totalpage;
		}

		$v_RSLT_ID = "100";
		//$v_RSLT_MSG = "Error: [$errno] $errstr";
		$v_RSLT_MSG = "There is something wrong. Please try again later.";
		
		//Pagination
		$v_links[] = array(
			'self'=>null,
			'first'=>null,
			'prev'=>null,
			'next'=>null,
			'last'=>null
		);
		
		$v_data[] = array(
			'registration_number'=>null,
			'status'=>null,
			'approval_date'=>null,
			'user_id'=>null,
			'activation'=>null,
			'activation_date'=>null
		);

		$response[] = array('registration_date'=>$v_registration_date, 'links'=>$v_links, 'limit'=>$v_limit, 'page'=>$v_page, 'totalpage'=>$v_totalpage, 'data'=>$v_data, 'result_id'=>$v_RSLT_ID, 'result_message'=>$v_RSLT_MSG);
		
		echo json_encode($response);
		die();
	}
	else {
		return true;
	}
}

//------------------------------------------------------------------
//Set Variable Constant
require_once('../config/validation.php');
require_once('../config/security.php');
$vc_pe_code = $sec_pe_code;
$vc_signature = $sec_signature;
$vc_api_type = "ActivationStatus";

$v_merchant_key = "STB";

//------------------------------------------------------------------
//Validation
if ($v_check && $v_pe_code != $vc_pe_code){
	$v_RSLT_ID = "20";
	$v_RSLT_MSG = "Invalid PE Code";
	$v_check = false;
}

if ($v_check && $v_signature != $vc_signature){
	$v_RSLT_ID = "21";
	$v_RSLT_MSG = "Invalid Signature";
	$v_check = false;
}

if ($v_check && $v_api_type != $vc_api_type){
	$v_RSLT_ID = "22";
	$v_RSLT_MSG = "Invalid API Type";
	$v_check = false;
}

if ($v_check && isValidStr($v_registration_date)!='OK'){
	$v_RSLT_ID = "24";
	$v_RSLT_MSG = "Invalid Input Parameter";
	$v_check = false;
}

//------------------------------------------------------------------
//Execute API process

//Pagination
$v_limit = 100;
$v_page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
if (($v_page < 1) || (empty($v_page))) {
	$v_page = 1;
}
//$v_offset = ($v_page > 1) ? (($v_page * $v_limit) - $v_limit) : 0;
//$v_offset = ($v_offset + 1);
$v_offset = (($v_page - 1) * $v_limit);
$v_seq = 0;

if ($v_check){
	require_once('../config/dbconn.php');
	mssql_select_db($db_web, $conn);
	
	mssql_query("SET ANSI_NULLS ON", $conn);
	mssql_query("SET ANSI_WARNINGS ON", $conn);
	
	$qryTotalReport = "exec API_ActivationStatusTotalData '$v_merchant_key', '$v_registration_date';";
	$execTotalReport = mssql_query($qryTotalReport, $conn);
	$numrowTotalReport = $execTotalReport ? mssql_num_rows($execTotalReport) : 0;
	
	$qryReport = "exec API_ActivationStatusPagination '$v_merchant_key', '$v_registration_date', '$v_limit', '$v_page', '$v_offset';";
	$execReport = mssql_query($qryReport, $conn);
	$numrowReport = $execReport ? mssql_num_rows($execReport) : 0;
	
	if (!$execReport || $numrowReport == 0) {
		//Pagination
		$v_totalpage = 1;
		if ($v_page > $v_totalpage) {
			$v_page = $v_totalpage;
			$v_RSLT_ID = "3";
			$v_RSLT_MSG = "Invalid Page";
		}
		
		//Pagination
		$v_links[] = array(
			'self'=>null,
			'first'=>null,
			'prev'=>null,
			'next'=>null,
			'last'=>null
		);
		
		$v_data[] = array(
			'registration_number'=>null,
			'status'=>null,
			'approval_date'=>null,
			'user_id'=>null,
			'activation'=>null,
			'activation_date'=>null
		);
	}
	else {
		//Pagination
		$v_totaldata = mssql_result($execTotalReport,0,"totaldata");
		$v_totalpage = ceil($v_totaldata / $v_limit);
		if ($v_page > $v_totalpage) {
			$v_page = $v_totalpage;
			$v_RSLT_ID = "3";
			$v_RSLT_MSG = "Invalid Page";
		}
		
		//Pagination
		$v_first = 1;
   		$v_prev = $v_page - 1;
		if ($v_prev < 1) {
			$v_prev = 1;
		}
		$v_next = $v_page + 1;
		if ($v_next > $v_totalpage) {
			$v_next = $v_totalpage;
		}
		$v_last = $v_totalpage;
		
		//Pagination
		if (isset($_GET['page'])) {
			$v_link_self = $v_url . '?page=' . (int)$_GET['page']; //. '&limit=' . $v_limit;
		}
		else {
			$v_link_self = $v_url;
		}
		$v_link_first = $v_url . '?page=' . $v_first; //. '&limit=' . $v_limit;
		$v_link_prev = $v_url . '?page=' . $v_prev; //. '&limit=' . $v_limit;
		$v_link_next = $v_url . '?page=' . $v_next; //. '&limit=' . $v_limit;
		$v_link_last = $v_url . '?page=' . $v_last; //. '&limit=' . $v_limit;
		
		//Pagination
		$v_links[] = array(
			'self'=>$v_link_self,
			'first'=>$v_link_first,
			'prev'=>$v_link_prev,
			'next'=>$v_link_next,
			'last'=>$v_link_last
		);
		
		while ($rowReport = mssql_fetch_assoc($execReport)){
			//Pagination
			$v_seq = $v_seq + 1;
			
			$v_data[] = array(
				'registration_number'=>$rowReport['registration_number'],
				'status'=>$rowReport['status'],
				'approval_date'=>$rowReport['approval_date'],
				'user_id'=>$rowReport['user_id'],
				'activation'=>$rowReport['activation'],
				'activation_date'=>$rowReport['activation_date']
			);
		}
	}
	
	$response[] = array('registration_date'=>$v_registration_date, 'links'=>$v_links, 'limit'=>$v_limit, 'page'=>$v_page, 'totalpage'=>$v_totalpage, 'data'=>$v_data, 'result_id'=>$v_RSLT_ID, 'result_message'=>$v_RSLT_MSG);
	
	echo json_encode($response);
	mssql_close($conn);
} else {
	//Pagination
	$v_totalpage = 1;
	if ($v_page > $v_totalpage) {
		$v_page = $v_totalpage;
		$v_RSLT_ID = "3";
		$v_RSLT_MSG = "Invalid Page";
	}
	
	//Pagination
	$v_links[] = array(
		'self'=>null,
		'first'=>null,
		'prev'=>null,
		'next'=>null,
		'last'=>null
	);

	$v_data[] = array(
		'registration_number'=>null,
		'status'=>null,
		'approval_date'=>null,
		'user_id'=>null,
		'activation'=>null,
		'activation_date'=>null
	);
	
	$response[] = array('registration_date'=>$v_registration_date, 'links'=>$v_links, 'limit'=>$v_limit, 'page'=>$v_page, 'totalpage'=>$v_totalpage, 'data'=>$v_data, 'result_id'=>$v_RSLT_ID, 'result_message'=>$v_RSLT_MSG);
	
	echo json_encode($response);
}
//------------------------------------------------------------------
?>
