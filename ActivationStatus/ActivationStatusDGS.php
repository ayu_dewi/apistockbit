<?php
//------------------------------------------------------------------
//Retrieve JSON File
header("Content-Type: application/json; charset=UTF-8");
$input = trim(file_get_contents("php://input"));
$datas = json_decode($input, false);

//------------------------------------------------------------------
//Set variable
foreach ($datas as $data)
{
	$v_pe_code = $data->pe_code;
	$v_signature = $data->signature;
	$v_api_type = $data->api_type;
	$v_option = $data->option;
	$v_opt_regdate = $data->opt_regdate;
	$v_opt_regnumber = $data->opt_regnumber;
	$v_opt_userid = $data->opt_userid;
	$v_opt_clientid = $data->opt_clientid;
	$v_opt_cifstatus = $data->opt_cifstatus;
	$v_opt_activation = $data->opt_activation;
	$v_opt_dgsregstatus = $data->opt_dgsregstatus;
	$v_opt_cifdocstatus = $data->opt_cifdocstatus;
	$v_opt_rdndocstatus = $data->opt_rdndocstatus;
}

$v_check = true;
$v_RSLT_ID = "0";
$v_RSLT_MSG = "";

//------------------------------------------------------------------
// Turn off all error reporting & handle exception
error_reporting(0);
set_error_handler("customError", E_ALL);

function customError($errno, $errstr)
{
	if (strlen(trim($errstr))>0) {
		$v_RSLT_ID = "100";
		//$v_RSLT_MSG = "Error: [$errno] $errstr";
		$v_RSLT_MSG = "Terjadi kendala pada server. Silakan dicoba beberapa saat lagi.";
		
		$v_data[] = array(
			'reg_date'=>null,
			'reg_number'=>null,
			'cif_status'=>null,
			'approval_date'=>null,
			'user_id'=>null,
			'activation_status'=>null,
			'activation_date'=>null,
			'client_id'=>null,
			'client_name'=>null,
			'client_nik'=>null,
			'client_mail'=>null,
			'dgs_reg_status'=>null,
			'dgs_reg_info'=>null,
			'cif_doc_id'=>null,
			'cif_doc_status'=>null,
			'cif_doc_info'=>null,
			'rdn_doc_id'=>null,
			'rdn_doc_status'=>null,
			'rdn_doc_info'=>null
		);
		$response[] = array(
			'option'=>$v_option,
			'opt_regdate'=>$v_opt_regdate, 
			'opt_regnumber'=>$v_opt_regnumber,
			'opt_userid'=>$v_opt_userid,
			'opt_clientid'=>$v_opt_clientid,
			'opt_cifstatus'=>$v_opt_cifstatus,
			'opt_activation'=>$v_opt_activation,
			'opt_dgsregstatus'=>$v_opt_dgsregstatus,
			'opt_cifdocstatus'=>$v_opt_cifdocstatus,
			'opt_rdndocstatus'=>$v_opt_rdndocstatus,
			'data'=>$v_data,
			'result_id'=>$v_RSLT_ID,
			'result_message'=>$v_RSLT_MSG
		);
		echo json_encode($response);
		die();
	}
	else {
		return true;
	}
}

//------------------------------------------------------------------
//Set Variable Constant
require_once('../config/validation.php');
require_once('../config/security.php');
$vc_pe_code = $sec_pe_code;
$vc_signature = $sec_signature;
$vc_api_type = "ActivationStatusDGS";

require_once('../config/dbconn.php');
mssql_select_db($db_web, $conn);

mssql_query("SET ANSI_NULLS ON", $conn);
mssql_query("SET ANSI_WARNINGS ON", $conn);

//------------------------------------------------------------------
//Validation
if ($v_pe_code != $vc_pe_code){
	$v_RSLT_ID = "20";
	$v_RSLT_MSG = "Invalid PE Code";
	$v_check = false;
}

if ($v_signature != $vc_signature){
	$v_RSLT_ID = "21";
	$v_RSLT_MSG = "Invalid Signature";
	$v_check = false;
}

if ($v_api_type != $vc_api_type){
	$v_RSLT_ID = "22";
	$v_RSLT_MSG = "Invalid API Type";
	$v_check = false;
}

if (isValidStr($v_opt_regdate)!='OK'){
	$v_RSLT_ID = "24";
	$v_RSLT_MSG = "Invalid Input Parameter";
	$v_check = false;
}

//$v_option : "REGDATE, REGNUM, USERID, CLIENTID, CIFSTATUS, ACTIVATIONSTATUS, DGSREGSTATUS, CIFDOCSTATUS, RDNDOCSTATUS"
if ($v_option == "" || !isset($v_option) || is_null($v_option) || $v_option == "null") {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty option";
	$v_check = false;
}
else if (strtoupper($v_option) != strtoupper("REGDATE") 
		&& strtoupper($v_option) != strtoupper("REGNUM") 
		&& strtoupper($v_option) != strtoupper("USERID") 
		&& strtoupper($v_option) != strtoupper("CLIENTID") 
		&& strtoupper($v_option) != strtoupper("CIFSTATUS") 
		&& strtoupper($v_option) != strtoupper("ACTIVATIONSTATUS") 
		&& strtoupper($v_option) != strtoupper("DGSREGSTATUS") 
		&& strtoupper($v_option) != strtoupper("CIFDOCSTATUS") 
		&& strtoupper($v_option) != strtoupper("RDNDOCSTATUS")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : invalid keyword option";
	$v_check = false;
}
if ($v_option == "REGDATE" && ($v_opt_regdate == "" || !isset($v_opt_regdate) || is_null($v_opt_regdate) || $v_opt_regdate == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_regdate";
	$v_check = false;
}
else if ($v_option == "REGNUM" && ($v_opt_regnumber == "" || !isset($v_opt_regnumber) || is_null($v_opt_regnumber) || $v_opt_regnumber == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_regnumber";
	$v_check = false;
}
else if ($v_option == "USERID" && ($v_opt_userid == "" || !isset($v_opt_userid) || is_null($v_opt_userid) || $v_opt_userid == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_userid";
	$v_check = false;
}
else if ($v_option == "CLIENTID" && ($v_opt_clientid == "" || !isset($v_opt_clientid) || is_null($v_opt_clientid) || $v_opt_clientid == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_clientid";
	$v_check = false;
}
else if ($v_option == "CIFSTATUS" && ($v_opt_cifstatus == "" || !isset($v_opt_cifstatus) || is_null($v_opt_cifstatus) || $v_opt_cifstatus == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_cifstatus";
	$v_check = false;
}
else if ($v_option == "CIFSTATUS" 
		&& strtoupper($v_opt_cifstatus) != strtoupper("REGCOMPLETE") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("RDNCREATED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("SIDCREATED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("PINACTIVATED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("ICAPPROVED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("ICREJECTED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("CSVERIFIED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("CSREJECTED") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("NEWREGIS") 
		&& strtoupper($v_opt_cifstatus) != strtoupper("NOTCOMPLETE")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : invalid keyword opt_cifstatus";
	$v_check = false;
}
else if ($v_option == "ACTIVATIONSTATUS" && ($v_opt_activation == "" || !isset($v_opt_activation) || is_null($v_opt_activation) || $v_opt_activation == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_activation";
	$v_check = false;
}
else if ($v_option == "DGSREGSTATUS" && ($v_opt_dgsregstatus == "" || !isset($v_opt_dgsregstatus) || is_null($v_opt_dgsregstatus) || $v_opt_dgsregstatus == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_dgsregstatus";
	$v_check = false;
}
else if ($v_option == "DGSREGSTATUS" 
		&& strtoupper($v_opt_dgsregstatus) != strtoupper("DONE") 
		&& strtoupper($v_opt_dgsregstatus) != strtoupper("VERIFICATION") 
		&& strtoupper($v_opt_dgsregstatus) != strtoupper("FAILED") 
		&& strtoupper($v_opt_dgsregstatus) != strtoupper("ONPROCESS")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : invalid keyword opt_dgsregstatus";
	$v_check = false;
}
else if ($v_option == "CIFDOCSTATUS" && ($v_opt_cifdocstatus == "" || !isset($v_opt_cifdocstatus) || is_null($v_opt_cifdocstatus) || $v_opt_cifdocstatus == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_cifdocstatus";
	$v_check = false;
}
else if ($v_option == "CIFDOCSTATUS" 
		&& strtoupper($v_opt_cifdocstatus) != strtoupper("DONE") 
		&& strtoupper($v_opt_cifdocstatus) != strtoupper("WAITINGSIGN") 
		&& strtoupper($v_opt_cifdocstatus) != strtoupper("FAILEDCHECKDOC") 
		&& strtoupper($v_opt_cifdocstatus) != strtoupper("SENTCHECKSTATUS") 
		&& strtoupper($v_opt_cifdocstatus) != strtoupper("FAILEDRESENDDOC") 
		&& strtoupper($v_opt_cifdocstatus) != strtoupper("ONPROCESS")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : invalid keyword opt_cifdocstatus";
	$v_check = false;
}
else if ($v_option == "RDNDOCSTATUS" && ($v_opt_rdndocstatus == "" || !isset($v_opt_rdndocstatus) || is_null($v_opt_rdndocstatus) || $v_opt_rdndocstatus == "null")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : empty opt_rdndocstatus";
	$v_check = false;
}
else if ($v_option == "RDNDOCSTATUS" 
		&& strtoupper($v_opt_rdndocstatus) != strtoupper("DONE") 
		&& strtoupper($v_opt_rdndocstatus) != strtoupper("WAITINGSIGN") 
		&& strtoupper($v_opt_rdndocstatus) != strtoupper("FAILEDCHECKDOC") 
		&& strtoupper($v_opt_rdndocstatus) != strtoupper("SENTCHECKSTATUS") 
		&& strtoupper($v_opt_rdndocstatus) != strtoupper("FAILEDRESENDDOC") 
		&& strtoupper($v_opt_rdndocstatus) != strtoupper("ONPROCESS")) {
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : invalid keyword opt_rdndocstatus";
	$v_check = false;
}

//------------------------------------------------------------------
//Execute API process
if ($v_check){
	$query = "exec API_ActivationStatusDGS 
				'$v_option', 
				'$v_opt_regdate', 
				'$v_opt_regnumber', 
				'$v_opt_userid', 
				'$v_opt_clientid', 
				'$v_opt_cifstatus', 
				'$v_opt_activation', 
				'$v_opt_dgsregstatus', 
				'$v_opt_cifdocstatus', 
				'$v_opt_rdndocstatus';";
	//echo $query . "\n";
	$exec = mssql_query($query, $conn);
	$numrow = mssql_num_rows($exec);
	
	if (!$exec || $numrow == 0) {
		$v_data[] = array(
			'reg_date'=>null,
			'reg_number'=>null,
			'cif_status'=>null,
			'approval_date'=>null,
			'user_id'=>null,
			'activation_status'=>null,
			'activation_date'=>null,
			'client_id'=>null,
			'client_name'=>null,
			'client_nik'=>null,
			'client_mail'=>null,
			'dgs_reg_status'=>null,
			'dgs_reg_info'=>null,
			'cif_doc_id'=>null,
			'cif_doc_status'=>null,
			'cif_doc_info'=>null,
			'rdn_doc_id'=>null,
			'rdn_doc_status'=>null,
			'rdn_doc_info'=>null
		);
	}
	else {
		while ($row = mssql_fetch_assoc($exec)){
			$v_data[] = array(
				'reg_date'=>$row['reg_date'],
				'reg_number'=>$row['reg_number'],
				'cif_status'=>$row['cif_status'],
				'approval_date'=>$row['approval_date'],
				'user_id'=>$row['user_id'],
				'activation_status'=>$row['activation_status'],
				'activation_date'=>$row['activation_date'],
				'client_id'=>$row['client_id'],
				'client_name'=>$row['client_name'],
				'client_nik'=>$row['client_nik'],
				'client_mail'=>$row['client_mail'],
				'dgs_reg_status'=>$row['dgs_reg_status'],
				'dgs_reg_info'=>$row['dgs_reg_info'],
				'cif_doc_id'=>$row['cif_doc_id'],
				'cif_doc_status'=>$row['cif_doc_status'],
				'cif_doc_info'=>$row['cif_doc_info'],
				'rdn_doc_id'=>$row['rdn_doc_id'],
				'rdn_doc_status'=>$row['rdn_doc_status'],
				'rdn_doc_info'=>$row['rdn_doc_info']
			);
		}
	}
	
	$response[] = array(
		'option'=>$v_option,
		'opt_regdate'=>$v_opt_regdate, 
		'opt_regnumber'=>$v_opt_regnumber,
		'opt_userid'=>$v_opt_userid,
		'opt_clientid'=>$v_opt_clientid,
		'opt_cifstatus'=>$v_opt_cifstatus,
		'opt_activation'=>$v_opt_activation,
		'opt_dgsregstatus'=>$v_opt_dgsregstatus,
		'opt_cifdocstatus'=>$v_opt_cifdocstatus,
		'opt_rdndocstatus'=>$v_opt_rdndocstatus,
		'data'=>$v_data,
		'result_id'=>$v_RSLT_ID,
		'result_message'=>$v_RSLT_MSG
	);
	
	echo json_encode($response);
	mssql_close($conn);
	
} else {
	$v_data[] = array(
		'reg_date'=>null,
		'reg_number'=>null,
		'cif_status'=>null,
		'approval_date'=>null,
		'user_id'=>null,
		'activation_status'=>null,
		'activation_date'=>null,
		'client_id'=>null,
		'client_name'=>null,
		'client_nik'=>null,
		'client_mail'=>null,
		'dgs_reg_status'=>null,
		'dgs_reg_info'=>null,
		'cif_doc_id'=>null,
		'cif_doc_status'=>null,
		'cif_doc_info'=>null,
		'rdn_doc_id'=>null,
		'rdn_doc_status'=>null,
		'rdn_doc_info'=>null
	);
	$response[] = array(
		'option'=>$v_option,
		'opt_regdate'=>$v_opt_regdate, 
		'opt_regnumber'=>$v_opt_regnumber,
		'opt_userid'=>$v_opt_userid,
		'opt_clientid'=>$v_opt_clientid,
		'opt_cifstatus'=>$v_opt_cifstatus,
		'opt_activation'=>$v_opt_activation,
		'opt_dgsregstatus'=>$v_opt_dgsregstatus,
		'opt_cifdocstatus'=>$v_opt_cifdocstatus,
		'opt_rdndocstatus'=>$v_opt_rdndocstatus,
		'data'=>$v_data,
		'result_id'=>$v_RSLT_ID,
		'result_message'=>$v_RSLT_MSG
	);
	echo json_encode($response);
}
//------------------------------------------------------------------
?>