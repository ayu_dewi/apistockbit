<?php
	function isValidStr($str) {
        $RSLT_MSG = 'OK';
		
		if ((strpos(strtolower($str), 'select') !== false) ||  
		   (strpos(strtolower($str), 'insert') !== false) || 
		   (strpos(strtolower($str), 'update') !== false) || 
		   (strpos(strtolower($str), 'delete') !== false) || 
		   (strpos(strtolower($str), 'from') !== false) || 
		   (strpos(strtolower($str), 'exec') !== false) || 
		   (strpos(strtolower($str), 'drop') !== false) || 
		   (strpos(strtolower($str), 'create') !== false) || 
		   (strpos(strtolower($str), 'alter') !== false) || 
		   (strpos(strtolower($str), 'script') !== false) || 
		   (strpos(strtolower($str), 'iframe') !== false) || 
		   (strpos(strtolower($str), '"') !== false) ||
		   (strpos(strtolower($str), "'") !== false) ||
		   (strpos(strtolower($str), 'truncate') !== false) ) 
		{
			$RSLT_MSG = 'NOOOOOOOO';
		}
		
        return $RSLT_MSG;
    }
?>